<?php

namespace Rapid\UserTypes;

class UserTypes
{
    public static function addEventHandlers()
    {
        self::registerUserFieldClasses();
        self::registerPropertyClasses();
    }

    private static function registerUserFieldClasses()
    {
        $arClasses = self::getUserFieldClasses();
        foreach ($arClasses as $className) {
            AddEventHandler('main', 'OnUserTypeBuildList', ['\Rapid\UserTypes\UserType\\' . $className, 'GetUserTypeDescription']);
        }
    }

    private static function registerPropertyClasses()
    {
        $arClasses = self::getPropertyClasses();
        foreach ($arClasses as $className) {
            AddEventHandler('iblock', 'OnIBlockPropertyBuildList', ['\Rapid\UserTypes\UserType\\' . $className, 'GetUserTypeDescription']);
        }
    }

    private static function getUserFieldClasses()
    {
        $arFiles = self::getUserTypeFiles();
        $arFiles = array_filter($arFiles, function($name){return strpos($name, 'CUserField') !== false;});
        array_walk($arFiles, function(&$name){$name = self::getFileName($name);});
        return $arFiles;
    }

    private static function getPropertyClasses()
    {
        $arFiles = self::getUserTypeFiles();
        $arFiles = array_filter($arFiles, function($name){return strpos($name, 'CProperty') !== false;});
        array_walk($arFiles, function(&$name){$name = self::getFileName($name);});
        return $arFiles;
    }

    private static function getUserTypeFiles()
    {
        $strParentDir = realpath(__DIR__ . '/UserType');
        $arFiles = scandir($strParentDir);
        $arFiles = array_diff($arFiles, ['.', '..']);
        return $arFiles;
    }

    protected static function getFileName($filename, $bBaseName = true)
    {
        if ($bBaseName) {
            $filename = basename($filename);
        }
        return substr($filename, 0, strrpos($filename, '.'));
    }
}
