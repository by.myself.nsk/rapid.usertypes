<?

namespace Rapid\UserTypes\UserType;

use Bitrix\Main\Web\Json;
use CJSCore;
use Rapid\Dev\Helper\SitePage;

class CPropertyTable
{
    protected static $defaultInputSize = 12;

    /**
     * @return int
     */
    public static function getDefaultInputSize(): int
    {
        return self::$defaultInputSize;
    }

    /**
     * @return int
     */
    public static function getInputSize($arProp): int
    {
        return $arProp['USER_TYPE_SETTINGS']['INPUT_SIZE'] ?: self::getDefaultInputSize();
    }

    /**
     * @return bool
     */
    public static function useWysiwyg($arProp): bool
    {
        $value = $arProp['USER_TYPE_SETTINGS']['USE_WYSIWYG'] ?: 'N';
        return $value == 'Y';
    }

    /**
     * @return bool
     */
    public static function useTextBefore($arProp): bool
    {
        $value = $arProp['USER_TYPE_SETTINGS']['SHOW_TEXT_BEFORE'] ?: 'N';
        return $value == 'Y';
    }

    /**
     * @return bool
     */
    public static function useTextAfter($arProp): bool
    {
        $value = $arProp['USER_TYPE_SETTINGS']['SHOW_TEXT_AFTER'] ?: 'N';
        return $value == 'Y';
    }

    /**
     * @return bool
     */
    public static function useColumnEdit($arProp): bool
    {
        $value = $arProp['USER_TYPE_SETTINGS']['USE_COLUMN_EDIT'] ?: 'N';
        return $value == 'Y';
    }

    protected static function getDataKeys()
    {
        return ['rows', 'cols', 'text_before', 'text_after'];
    }

    public function GetUserTypeDescription()
    {
        if(SitePage::isAdmin()) {
            static::showCss();
            static::showJs();
        }
        $arData = [
            'PROPERTY_TYPE' => 'S',
            'USER_TYPE' => 'table',
            'DESCRIPTION' => 'R: Таблица',
        ];
        foreach ([
            'ConvertToDB',
            'ConvertFromDB',
            'GetPropertyFieldHtml',
            'GetAdminListViewHTML',
            'PrepareSettings', // вызывается перед сохранением настроек свойства (USER_TYPE_SETTINGS)
            'GetSettingsHTML', // вызывается при отображении настроек свойства
        ] as $strMethodName) {
            $arData[$strMethodName] = array(static::class, $strMethodName);
        }
        return $arData;
    }

    public static function getPropertyCode($arHTMLControlNames = [])
    {
        return $arHTMLControlNames['VALUE'];
    }

    protected static function prepareHtmlFieldName($arProp, $strName)
    {
        return static::useWysiwyg($arProp) ? preg_replace("/([^a-z0-9])/is", "_", $strName) : $strName;
    }

    protected static function getHtmlField($arProp, $arHTMLControlNames, $val, $key)
    {
        $arHtmlVal = &$val['VALUE'][$key];
        if (!static::useWysiwyg($arProp)) {
            ob_start();
            ?>
            <textarea name="<?=$arHTMLControlNames['VALUE'].'['.$key.'][TEXT]'?>" id="" cols="30" rows="10"><?=\htmlspecialcharsbx($arHtmlVal['TEXT'])?></textarea>
            <?
            return ob_get_clean();
        }
        if (!\CModule::IncludeModule('fileman')) {
            return GetMessage('IBLOCK_PROP_HTML_NOFILEMAN_ERROR');
        }
        ob_start();

        $strTextFieldName = static::prepareHtmlFieldName($arProp, $arHTMLControlNames['VALUE'].'['.$key.'][TEXT]');
        $strTextValue = $arHtmlVal['TEXT'];
        $strTextTypeFieldName = static::prepareHtmlFieldName($arProp, $arHTMLControlNames['VALUE'].'['.$key.'][TYPE]');
        $strTextTypeValue = $arHtmlVal['VALUE'];

        \CFileMan::AddHTMLEditorFrame(
            $strTextFieldName,
            $strTextValue,
            $strTextTypeFieldName,
            $strTextTypeValue,
            array(
                'height' => 100,
                'width'  => '100%'
            )
        );

        if ($arProp['MULTIPLE'] == 'Y') {
            echo '<input type="hidden" name="' . $arHTMLControlNames["NAME"] . '" >';
        }
        return ob_get_clean();
    }

    protected static function getTableHead($arProp, $arHTMLControlNames, $arCols, $bEdit = true)
    {
        ob_start();
        ?>
        <tr class="js-matrix-head">
            <?
            if (is_array($arCols)) {
                foreach ($arCols as $strColCode => $strColName) {
                    $bPrice = $strColCode === 'price';
                    ?>
                    <td>
                        <?
                        if ($bEdit) {
                            ?>
                            <input type="text" size="<?=static::getInputSize($arProp)?>"
                                placeholder="Параметр"
                                name="<?=static::getPropertyCode($arHTMLControlNames)?>[cols][<?=$strColCode?>]"
                                value="<?=$strColName?>"
                                data-code="<?=$strColCode?>"
                                <?=$bPrice ? ' readonly tabindex="-1"' : ''?>
                            >
                            <?
                        } else {
                            echo $strColName;
                        }
                        if ($bEdit && !$bPrice && static::useColumnEdit($arProp)) {
                            ?>
                            <input type="button" value="x" class="js-remove-col-btn" tabindex="-1">
                            <?
                        }
                        ?>
                    </td>
                    <?
                }
            }
            if ($bEdit && static::useColumnEdit($arProp)) {
                ?>
                <td><input type="button" value="Добавить столбец" class="js-add-col-btn" tabindex="-1"></td>
                <?
            }
            ?>
        </tr>
        <tr class="js-hr">
            <?
            foreach ($arCols as $strColCode => $strColName) {
                ?>
                <td><hr></td>

                <?
            }
            ?>
            <td><hr></td>
        </tr>
        <?
        return ob_get_clean();
    }

    protected static function getHtmlRow($arProp, $arHTMLControlNames, $val, $key, $bEdit = true)
    {
        ob_start();
        ?>
        <tr>
            <td colspan="3" style="padding:10px 0;">
            <?
                if ($bEdit) {
                    ?>
                    <div class="spoiler js-spoiler">
                        <div>
                            <a href="#" class="show-spoiler js-show-spoiler">Показать поле для текста</a>
                            <a href="#" class="hide-spoiler js-hide-spoiler">Скрыть поле для текста</a>
                        </div>
                        <div class="spoiler-content js-spoiler-content">
                            <?
                            echo static::getHtmlField($arProp, $arHTMLControlNames, $val, $key);
                            ?>
                        </div>
                    </div>
                    <?

                } else {
                    echo \FormatText($val);
                }
            ?>
            </td>
        </tr>
        <?
        return ob_get_clean();
    }

    protected static function getTableRow(
        $arProp,
        $arHTMLControlNames,
        $arCols,
        $strRowCode = '',
        $arRow = array(),
        $bEdit = true
    )
    {
        ob_start();
        ?>
        <tr class="js-row">
            <?
            foreach ($arCols as $strColCode => $strColName) {
                ?>
                <td>
                    <?
                    if ($bEdit) {
                        ?>
                        <input type="text" size="<?=static::getInputSize($arProp)?>"
                            name="<?=static::getPropertyCode($arHTMLControlNames)?>[rows][<?=$strRowCode?>][<?=$strColCode?>]"
                            value="<?=$arRow[$strColCode]?>"
                            data-code="<?=$strColCode?>"
                        >
                        <?
                    } else {
                        echo $arRow[$strColCode];
                    }
                    ?>
                </td>
                <?
            }
            if ($bEdit) {
                ?>
                <td valign="bottom"><input type="button" value="Удалить строку" class="js-remove-row-btn" tabindex="-1"></td>
                <?
            }
            ?>
        </tr>
        <?
        return ob_get_clean();
    }

    public static function showCss()
    {
        static $shown = false;
        if ($shown) {
            return;
        }
        ?>
        <!--CPropertyTariff-->
        <style>
            .js-matrix-head td {
                min-width:180px;
            }
            .spoiler .show-spoiler{
                display: inline-block;
                text-decoration: none;
                border-bottom: 1px dashed;
            }
            .spoiler .hide-spoiler,
            .spoiler .spoiler-content{
                display: none;
            }
            .spoiler.opened .show-spoiler{
                display: none;
            }
            .spoiler.opened .hide-spoiler{
                display: inline-block;
                text-decoration: none;
                border-bottom: 1px dashed;
            }
            .spoiler.opened .spoiler-content{
                display: block;
            }
        </style>
        <?
        $shown = true;
    }

    public static function showJs()
    {
        static $shown = false;
        if (!$shown) {
            CJSCore::Init(array("jquery"));
            ?>
            <script>
                var propertyWrapperSelector = '.js-matrix-body-tariff';

                function recalcInputNames($wrapper, propertyCode)
                {
                    $wrapper.find('tr:not(.js-hr)').each(function(rowIndex){
                        var $row = $(this);
                        if (!$row.hasClass('js-matrix-head') && !$row.hasClass('js-row')) {
                            return;
                        }
                        var $inputs = $row.find('input[type="text"], select, textarea');
                        var count = $inputs.length;

                        $inputs.each(function(colIndex, elem){
                            var $input = $(this);

                            var code = $input.data('code') ? $input.data('code') : colIndex;

                            var isTableHead = $row.hasClass('js-matrix-head');

                            if (isTableHead) {
                                $input.prop('name', propertyCode + '[cols][' + code + ']');
                            } else {
                                $input.prop('name', propertyCode + '[rows][' + rowIndex + '][' + code + ']');
                            }
                        });
                    });
                }

                // работа со строками
                function countRows($wrapper) {
                    return $wrapper.find('tr.js-row').length;
                }

                function disableFirstRemoveRowBtn($wrapper) {
                    $($wrapper.find('input.js-remove-row-btn').get(0)).prop('disabled', true);
                }

                function enableRemoveRowBtn($wrapper) {
                    $wrapper.find('input.js-remove-row-btn').prop('disabled', false);
                }

                // работа со столбцами
                function countRemoveColBtns($wrapper) {
                    return $wrapper.find('.js-remove-col-btn').length;
                }

                function disableFirstRemoveColBtn($wrapper) {
                    $($wrapper.find('input.js-remove-col-btn').get(0)).prop('disabled', true);
                }

                function enableRemoveColBtns($wrapper) {
                    $wrapper.find('input.js-remove-col-btn').prop('disabled', false);
                }

                function countRemoveWrapperBtns($wrapper) {
                    return $wrapper.find('.js-remove-wrapper-btn').length;
                }

                function disableFirstRemoveWrapperBtn($wrapper) {
                    $($wrapper.find('input.js-remove-wrapper-btn').get(0)).prop('disabled', true);
                }

                function enableRemoveWrapperBtns($wrapper) {
                    $wrapper.find('input.js-remove-wrapper-btn').prop('disabled', false);
                }

                function checkBtns()
                {
                    var $wrappers = $(propertyWrapperSelector).parents('.adm-detail-content-cell-r');
                    $wrappers.each(function(){
                        var $commonWrapper = $(this);

                        if (countRemoveWrapperBtns($commonWrapper) < 2) {
                            disableFirstRemoveWrapperBtn($commonWrapper);
                        } else {
                            enableRemoveWrapperBtns($commonWrapper);
                        }
                    });
                }

                $(function () {
                    // если элемент является блоком конструктора, то форма редактирования показывается в модалке, и при каждом открытии скрипт подтягивается снова.
                    window.propertyTableInitialized = window.propertyTableInitialized || false;
                    if (window.propertyTableInitialized) {
                        return;
                    }
                    window.propertyTableInitialized = true;

                    var $body = $('body');

                    function isMultiple($wrapper)
                    {
                        var $commonWrapper = $wrapper.closest('.adm-detail-content-cell-r');
                        return $commonWrapper.find(propertyWrapperSelector).length > 1;
                    }

                    $(propertyWrapperSelector).each(function(){
                        var $wrapper = $(this);
                        var propertyCode = $wrapper.data('propertyName');
                        recalcInputNames($wrapper, propertyCode);
                        if (countRows($wrapper) < 2) {
                            disableFirstRemoveRowBtn($wrapper);
                        }
                        if (countRemoveColBtns($wrapper) < 2) {
                            disableFirstRemoveColBtn($wrapper);
                        }
                    });

                    $body.on('click', '.js-add-row-btn', function (e) {
                        e.preventDefault();
                        var $this = $(this);
                        var $wrapper = $this.closest('table').find(propertyWrapperSelector);

                        var propertyCode = $wrapper.data('propertyName');
                        var $row = $wrapper.find('tr.js-row').last().clone();
                        $wrapper.append($row);
                        enableRemoveRowBtn($wrapper);
                        recalcInputNames($wrapper, propertyCode);
                    });

                    $body.on('click', '.js-remove-row-btn', function (e) {
                        e.preventDefault();

                        var $this = $(this);
                        var $wrapper = $this.closest(propertyWrapperSelector);
                        var propertyCode = $wrapper.data('propertyName');

                        if (countRows($wrapper) >= 2) {
                            $this.parents('tr.js-row').remove();
                        }
                        if (countRows($wrapper) < 2) {
                            disableFirstRemoveRowBtn($wrapper);
                        }
                        recalcInputNames($wrapper, propertyCode);
                    });

                    $body.on('click', '.js-add-col-btn', function (e) {
                        e.preventDefault();
                        var $this = $(this);
                        var $wrapper = $this.closest(propertyWrapperSelector);
                        var propertyCode = $wrapper.data('propertyName');

                        $wrapper.find('tr').each(function(){
                            var $row = $(this);
                            var $clone = $row.find('td:first-child').clone();
                            $clone.find('input[type="text"]').val('');
                            var $price = $row.find('td').eq(-2);
                            $clone.insertBefore($price);
                        });
                        enableRemoveColBtns($wrapper);
                        recalcInputNames($wrapper, propertyCode);
                    });

                    $body.on('click', '.js-remove-col-btn', function (e) {
                        e.preventDefault();
                        var $this = $(this);
                        var $wrapper = $this.closest(propertyWrapperSelector);
                        var propertyCode = $wrapper.data('propertyName');

                        if (countRemoveColBtns($wrapper) >= 2) {
                            // удалить в этой строке (шапке) и в каждой последующей (с данными)
                            var $row = $this.closest('tr');// строка
                            var $cell = $this.closest('td');// ячейка
                            var index = $row.find('td').index($cell);
                            $wrapper.find('tr').each(function(){$(this).find('td').eq(index).remove();});
                        }
                        if (countRemoveColBtns($wrapper) < 2) {
                            disableFirstRemoveColBtn($wrapper);
                        }
                        recalcInputNames($wrapper, propertyCode);
                    });

                    // удаление таблицы у множественного свойства
                    $body.on('click', '.js-remove-wrapper-btn', function(e){
                        e.preventDefault();
                        var $this = $(this);
                        var $commonWrapper = $this.closest('.adm-detail-content-cell-r');
                        var $item = $this.closest('table').closest('tr');
                        $item.remove();
                        if (countRemoveWrapperBtns($commonWrapper) < 2) {
                            disableFirstRemoveWrapperBtn($commonWrapper);
                        }
                    });

                    // спойлеры для html-блоков
                    $body.on('click', '.js-show-spoiler', function(e){
                        e.preventDefault();
                        var $this = $(this);
                        var $spoiler = $this.closest('.js-spoiler');
                        $spoiler.addClass('opened');
                    });
                    $body.on('click', '.js-hide-spoiler', function(e){
                        e.preventDefault();
                        var $this = $(this);
                        var $spoiler = $this.closest('.js-spoiler');
                        $spoiler.removeClass('opened');
                    });
                });

                BX.ready(function(){
                    var needRefreshBtns = false;
                    BX.addCustomEvent('onAddNewRowBeforeInner',function(data) {
                        var $html = $(data.html);

                        var $wrapper = $html.find(propertyWrapperSelector);
                        if (!$wrapper.length) {
                            return;
                        }
                        // разблокировать кнопку "Удалить таблицу"
                        needRefreshBtns = true;

                        // найти name всех инпутов и поправить в них id, подменив на n0
                        data.html = data.html.replace( /(PROP\[\d+\]\[)(\d+)(\])/g, "$1n0$3" );
                    });
                    BX.addCustomEvent('onAdminTabsChange',function() {
                        if (needRefreshBtns) {
                            checkBtns();
                            needRefreshBtns = false;
                        }
                    });
                });
            </script>
            <?
        }
        $shown = true;
    }

    public static function checkValue($val, $bEdit = true)
    {
        if (!is_array($val['VALUE']['cols'])) {
            $val['VALUE']['cols'] = array();
        }

        if ($bEdit) {
            if (!$val['VALUE']['cols']['price']) {
                $val['VALUE']['cols']['price'] = 'Цена';
            }
            if (count($val['VALUE']['cols']) < 2) {
                // добавить в начало 1 столбец
                $val['VALUE']['cols'] = array_merge(array(''), $val['VALUE']['cols']);
            }
        }

        if (!is_array($val['VALUE']['rows'])) {
            $val['VALUE']['rows'] = array();
        }
        if (!count($val['VALUE']['rows'])) {
            $val['VALUE']['rows'][] = \array_fill_keys(array_keys($val['VALUE']['cols']), '');
        }
        return $val;
    }

    public function GetPropertyFieldHtml($arProp, $val, $arHTMLControlNames)
    {
        ob_start();
        CJSCore::Init(array('jquery'));
        $strCode = static::getPropertyCode($arHTMLControlNames);
        $bEmptyItemProp = !is_array($val['VALUE']);
        $bTriedSave = $_POST['apply'] || $_POST['save'];
        if ($bTriedSave && !empty($_POST[$strCode])) {
            $val['VALUE'] = static::prepareAfterLoad(static::prepareBeforeSave($val));
            $bEmptyItemProp = !is_array($val['VALUE']);
        }
        $arData = &$val['VALUE'];
        if (!$bTriedSave && $bEmptyItemProp) {
            $arData = static::prepareAfterLoad($val);
        }
        $val = static::checkValue($val); // если не хватает данных, поправить это
        ?><div style="padding-top:10px;"></div><?
        if ($arProp['WITH_DESCRIPTION'] == 'Y' && trim($arHTMLControlNames['DESCRIPTION'])) {
            ?><input type="text" placeholder="Заголовок таблицы" size="40" name="<?=$arHTMLControlNames['DESCRIPTION']?>" value="<?=htmlspecialcharsbx($val['DESCRIPTION'])?>"><?
        }
        ?>
        <table>
            <tbody class="js-matrix-body-tariff" data-property-name="<?=static::getPropertyCode($arHTMLControlNames)?>">
            <?
            if (static::useTextBefore($arProp)) {
                echo static::getHtmlRow($arProp, $arHTMLControlNames, $val, 'text_before');
            }
            echo static::getTableHead($arProp, $arHTMLControlNames, $val['VALUE']['cols'], true);
            if (!empty($val['VALUE']['rows'])) {
                foreach ($val['VALUE']['rows'] as $key => $row) {
                    echo static::getTableRow($arProp, $arHTMLControlNames, $val['VALUE']['cols'], $key, $row);
                }
            } else {
                echo static::getTableRow($arProp, $arHTMLControlNames, $val['VALUE']['cols']);
            }
            ?>
            </tbody>
            <tfoot>
            <?
            if (static::useTextAfter($arProp)) {
                echo static::getHtmlRow($arProp, $arHTMLControlNames, $val, 'text_after');
            }
            ?>
            <tr>
                <td colspan="3" style="padding:10px 0;">
                    <input type="button" value="Добавить строку" class="js-add-row-btn">
                    <?
                    if ($arProp['MULTIPLE'] == 'Y') { // если множественное свойство
                        ?>
                        <input type="button" class="js-remove-wrapper-btn" value="Удалить таблицу">
                        <?
                    }
                    ?>
                </td>
            </tr>
            </tfoot>
        </table>
        <?
        return ob_get_clean();
    }

    public function getAdminListViewHTML($arProp, $val, $arHTMLControlNames)
    {
        ob_start();
        CJSCore::Init(array('jquery'));

        $bEmptyItemProp = !is_array($val['VALUE']);

        $arData = &$val['VALUE'];
        $val = static::checkValue($val);
        ?>
        <table>
            <tbody class="js-matrix-body-tariff">
            <?

            echo static::getTableHead($arProp, $arHTMLControlNames, $val['VALUE']['cols'], false);
            if (!empty($val['VALUE']['rows'])) {
                foreach ($val['VALUE']['rows'] as $key => $row) {
                    echo static::getTableRow($arProp, $arHTMLControlNames, $val['VALUE']['cols'], $key, $row, false);
                }
            } else {
                echo static::getTableRow($arProp, $arHTMLControlNames, $val['VALUE']['cols'], '', [], false);
            }
            ?>
            </tbody>
        </table>
        <?
        return ob_get_clean();
    }

    protected static function convertPostToRows($arData)
    {
        if (!is_array($arData)) {
            return false;
        }

        $arData = array_intersect_key($arData, array_flip(static::getDataKeys()));
        foreach ($arData['rows'] as $key => &$arItem) {
            if (!is_array($arItem)) {
                unset($arData['rows'][$key]);
                continue;
            }
            $arItem = array_intersect_key($arItem, $arData['cols']);
        };
        unset($arItem);
        return $arData;
    }

    // почему было вынесено в отдельный метод:
    // потому что элемент могли пытаться сохранить, но он не был сохранен. Форма должна быть заполненной.
    // соответственно, метод использовался для вывода в таких случаях

    public static function prepareBeforeSave($val)
    {
        return Json::encode(
                $val,
            JSON_HEX_TAG
            | JSON_HEX_AMP
            | JSON_HEX_APOS
            | JSON_HEX_QUOT
            | JSON_UNESCAPED_UNICODE
            | JSON_PRETTY_PRINT
        );
    }

    protected static function convertHtmlData($arProp, $val)
    {
        // сделано на основе стандартного кода из битрикса (/bitrix/modules/iblock/admin/iblock_element_edit.php, line ~548)
        // в исходном коде смысл заключался в следующем:
        // пробегались по POST, но по той части, свойства которых уже были преобразованы (проверка is_array())
        // затем строили ключ нужного вида, и если он находился в POST, то читали из POST значения и складывали нужным образом в VALUE
        // сейчас проблема во множественности свойства. Мы не имеем данных об id свойства
        // этот код надо выполнять заранее перед тем, как отрабатывает битриксовый код. Как это сделать - пока тоже понятно.

        if (!isset($_POST['PROP']) || !is_array($_POST['PROP'])) {
            return $val;
        }
        $arKeys = ['text_before', 'text_after'];
        foreach ($_POST['PROP'] as $strPropId => $propVal) {
            if (!is_array($propVal)) {
                continue;
            }
            foreach ($propVal as $propValId => $arVal) {
                // если структура не похожа на структуру свойства, игнорим
                if (!isset($arVal['VALUE']) || !is_array($arVal['VALUE'])
                    || !isset($arVal['VALUE']['cols']) || !is_array($arVal['VALUE']['cols'])
                    || !isset($arVal['VALUE']['rows']) || !is_array($arVal['VALUE']['rows'])
                ) {
                    continue;
                }
                foreach ($arKeys as $strValKey) {
                    $text_name = preg_replace('/([^a-z0-9])/is', '_', 'PROP['.$strPropId.']['.$propValId.'][VALUE]['.$strValKey.'][TEXT]');
                    if (!array_key_exists($text_name, $_POST)) {
                        continue;
                    }
                    //'PROP_79__457__VALUE__text_before__TYPE_'
                    // PROP_79__457__VALUE__text_before__TEXT_
                    $text_name = preg_replace('/([^a-z0-9])/is', '_', 'PROP['.$strPropId.']['.$propValId.'][VALUE]['.$strValKey.'][TEXT]');
                    $type_name = preg_replace('/([^a-z0-9])/is', '_', 'PROP['.$strPropId.']['.$propValId.'][VALUE]['.$strValKey.'][TYPE]');
                    // тут проблема во множественности свойства. Мы не имеем данных об id свойства
//                    $val['VALUE'][$strValKey]['TEXT'] = $_POST[$text_name];
//                    $val['VALUE'][$strValKey]['TYPE'] = $_POST[$type_name];
                    $_POST['PROP'][$strPropId][$propValId]['VALUE'][$strValKey]['TEXT'] = $_POST[$text_name];
                    $_POST['PROP'][$strPropId][$propValId]['VALUE'][$strValKey]['TYPE'] = $_POST[$type_name];
                }
            }
        }
        return $val;
    }

    public function ConvertToDB($arProp, $val)
    {
        $val = static::checkBeforeSave($arProp, $val, $arErrors);
        if (is_array($arErrors) && !empty($arErrors)) {
            return null; // не сохраняем, если нет ни одного значения в строках
        }
        $val['VALUE'] = static::prepareBeforeSave($val['VALUE']);
        return $val;
    }

    protected static function prepareAfterLoad($val)
    {
        if (empty($val)) {
            $data = array();
        } else {
            $data = json_decode($val, true);
        }
        if (!$data['cols']) {
            $data['cols'] = array();
        }
        if (!$data['rows']) {
            $data['rows'] = array();
        }
        if (!$data['text_before']) {
            $data['text_before'] = '';
        }
        if (!$data['text_after']) {
            $data['text_after'] = '';
        }
        $data = \array_intersect_key($data, array_flip(static::getDataKeys()));
        return $data;
    }

    public function ConvertFromDB($arProp, $val)
    {
        $val['VALUE'] = static::prepareAfterLoad($val['VALUE']);
        return $val;
    }

    public static function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
    {
        $arPropertyFields = [
            'HIDE' => [
                'FILTRABLE', 'ROW_COUNT', 'COL_COUNT', 'SMART_FILTER'
            ],
            'SHOW' => [
            ],
            'SET' => [
                'FILTRABLE' => 'N',
                'SMART_FILTER' => 'N',

            ],
        ];

        ob_start();
        ?>
        <tr>
            <td width="40%" class="adm-detail-content-cell-l">
                <label for="PROPERTY_MULTIPLE_Y">Размер поля для ввода значения:</label>
            </td>
            <td class="adm-detail-content-cell-r">
                <input type="text" size="5" name="<?=$strHTMLControlName["NAME"]?>[INPUT_SIZE]" value="<?=$arProperty['USER_TYPE_SETTINGS']['INPUT_SIZE'] ?: self::getDefaultInputSize()?>">
            </td>
        </tr>
        <tr>
            <td width="40%" class="adm-detail-content-cell-l">
                <label for="SHOW_USE_COLUMN_EDIT_Y">Разрешить редактировать колонки:</label>
            </td>
            <td class="adm-detail-content-cell-r">
                <input type="hidden" id="USE_COLUMN_EDIT_N" name="<?=$strHTMLControlName["NAME"]?>[USE_COLUMN_EDIT]" value="N" checked>
                <input type="checkbox" id="USE_COLUMN_EDIT_Y" name="<?=$strHTMLControlName["NAME"]?>[USE_COLUMN_EDIT]" value="Y"
                        class="adm-designed-checkbox"<?=$arProperty['USER_TYPE_SETTINGS']['USE_COLUMN_EDIT'] == 'Y' ? ' checked' : ''?>
                >
                <label class="adm-designed-checkbox-label" for="USE_COLUMN_EDIT_Y" title=""></label>
            </td>
        </tr>
        <tr>
            <td width="40%" class="adm-detail-content-cell-l">
                <label for="SHOW_TEXT_BEFORE_Y">Показывать текст перед таблицей:</label>
            </td>
            <td class="adm-detail-content-cell-r">
                <input type="hidden" id="SHOW_TEXT_BEFORE_N" name="<?=$strHTMLControlName["NAME"]?>[SHOW_TEXT_BEFORE]" value="N" checked>
                <input type="checkbox" id="SHOW_TEXT_BEFORE_Y" name="<?=$strHTMLControlName["NAME"]?>[SHOW_TEXT_BEFORE]" value="Y"
                        class="adm-designed-checkbox"<?=$arProperty['USER_TYPE_SETTINGS']['SHOW_TEXT_BEFORE'] == 'Y' ? ' checked' : ''?>
                >
                <label class="adm-designed-checkbox-label" for="SHOW_TEXT_BEFORE_Y" title=""></label>
            </td>
        </tr>
        <tr>
            <td width="40%" class="adm-detail-content-cell-l">
                <label for="SHOW_TEXT_AFTER_Y">Показывать текст после таблицы:</label>
            </td>
            <td class="adm-detail-content-cell-r">
                <input type="hidden" id="SHOW_TEXT_AFTER_N" name="<?=$strHTMLControlName["NAME"]?>[SHOW_TEXT_AFTER]" value="N" checked>
                <input type="checkbox" id="SHOW_TEXT_AFTER_Y" name="<?=$strHTMLControlName["NAME"]?>[SHOW_TEXT_AFTER]" value="Y"
                        class="adm-designed-checkbox"<?=$arProperty['USER_TYPE_SETTINGS']['SHOW_TEXT_AFTER'] == 'Y' ? ' checked' : ''?>
                >
                <label class="adm-designed-checkbox-label" for="SHOW_TEXT_AFTER_Y" title=""></label>
            </td>
        </tr>
        <tr>
            <td width="40%" class="adm-detail-content-cell-l">
                <label for="USE_WYSIWYG_Y">Использовать визуальный редактор дле текстов перед таблицей и после таблицы:</label>
            </td>
            <td class="adm-detail-content-cell-r">
                <input type="hidden" id="USE_WYSIWYG_N" name="<?=$strHTMLControlName["NAME"]?>[USE_WYSIWYG]" value="N" checked>
                <input type="checkbox" id="USE_WYSIWYG_Y" name="<?=$strHTMLControlName["NAME"]?>[USE_WYSIWYG]" value="Y"
                        class="adm-designed-checkbox"<?=$arProperty['USER_TYPE_SETTINGS']['USE_WYSIWYG'] == 'Y' ? ' checked' : ''?>
                >
                <label class="adm-designed-checkbox-label" for="USE_WYSIWYG_Y" title=""></label>
            </td>
        </tr>
        <?
        return ob_get_clean();
    }

    protected static function checkBeforeSave($arProp, $val, &$arErrors = [])
    {
        $arErrors = [];
        $bEmpty = true;
        if (!is_array($val) || !is_array($val['VALUE']) || !array_key_exists('rows', $val['VALUE']) || !array_key_exists('cols', $val['VALUE'])
            || !is_array($val['VALUE']['rows']) || empty($val['VALUE']['rows'])
        ) {
            $arErrors[] = 'Таблица: неверная структура данных';
        }
        if (is_array($val['VALUE']) && is_array($val['VALUE']['rows'])) {
            if (!empty($val['VALUE']['rows'])) {
                foreach ($val['VALUE']['rows'] as $intRowId => $arRow) {
                    $arRow = array_filter($arRow);
                    if (!empty($arRow)) {
                        $bEmpty = false;
//                        break; // не нужно, иначе  не удалятся пустые строки
                    } else {
                        unset($val['VALUE']['rows'][$intRowId]);
                    }
                }
            }
        }
        if (static::useTextBefore($arProp) && !empty($val['VALUE']['text_before']['TEXT'])) {
            $bEmpty = false;
        }
        if (static::useTextAfter($arProp) && !empty($val['VALUE']['text_after']['TEXT'])) {
            $bEmpty = false;
        }
        if ($bEmpty) {
            $arErrors[] = 'Таблица: данные не заполнены';
        }
        return $bEmpty ? null : $val;
    }

    public static function CheckFields($arProp, $val)
    {
        if (static::useWysiwyg($arProp)) {
            $val = static::convertHtmlData($arProp, $val);
        }
        return [];

//        static::checkBeforeSave($val, $arErrors);
//        return $arErrors;
    }

    public static function PrepareSettings($arFields)
    {
        $userTypeSettings = $arFields['USER_TYPE_SETTINGS'];
        $userTypeSettings['INPUT_SIZE'] = (int)$userTypeSettings['INPUT_SIZE'] ?: self::getDefaultInputSize();
        $userTypeSettings['USE_COLUMN_EDIT'] = $userTypeSettings['USE_COLUMN_EDIT'] == 'Y' ?: 'N';
        $userTypeSettings['SHOW_TEXT_BEFORE'] = $userTypeSettings['SHOW_TEXT_BEFORE'] == 'Y' ?: 'N';
        $userTypeSettings['SHOW_TEXT_AFTER'] = $userTypeSettings['SHOW_TEXT_AFTER'] == 'Y' ?: 'N';
        $userTypeSettings['USE_WYSIWYG'] = $userTypeSettings['USE_WYSIWYG'] == 'Y' ?: 'N';
        return $userTypeSettings;
    }
}
