<?

namespace Rapid\UserTypes\UserType;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class CPropertySiteId
{
    public function GetUserTypeDescription()
    {
        return array(
            "PROPERTY_TYPE" => 'S',
            "USER_TYPE" => 'SiteId',
            'DESCRIPTION'  => 'R: Привязка к сайту',
            "GetPropertyFieldHtml" => array(__CLASS__, "GetPropertyFieldHtml"),
            "GetPublicViewHTML" => array(__CLASS__, "GetPublicViewHTML"),
            "GetSettingsHTML" => array(__CLASS__, "GetSettingsHTML"),
            "GetAdminListViewHTML" => array(__CLASS__, "GetAdminListViewHTML"),
        );
    }

    protected static function getList()
    {
        static $arItems = array();
        static $bInited = false;
        if ($bInited) {
            return $arItems;
        }
        $by = 'C_SORT';
        $order = 'ASC';

        $dbItems = \CSite::GetList(
        $by,
        $order,
            []
        );

        while ($arItem = $dbItems->Fetch()) {
            $arItems[$arItem['ID']] = '['.$arItem['ID'].'] '.$arItem['NAME'];
        }
        $bInited = true;
        return $arItems;
    }

    protected static function getValueName($valId)
    {
        $arValues = static::getList();
        return $arValues[$valId];
    }

    protected static function showSelect($arProperty, $value, $strHTMLControlName)
    {
        if(($intSize = intval($arProperty["USER_TYPE_SETTINGS"]["size"])) > 1) {
            $size = ' size="'.$intSize.'"';
        } else {
            $size = '';
        }
        $currentOptionValue = $value['VALUE'];

        $strControlName = ' name="'.$strHTMLControlName["VALUE"].'"';

        if ($arProperty['LIST_TYPE'] == 'L') {
            ?>
            <select<?=$strControlName?><?=$size?>>
            <?
                $bSelectedExists = false;
                ob_start();
                foreach ($arProperty['VALUES'] as $strOptionValue => $strOptionName) {
                    if ($arProperty['MULTIPLE'] == 'Y') {
                        if (!is_array($currentOptionValue)) {
                            $currentOptionValue = array($currentOptionValue);
                        }
                        $bSelected = in_array($strOptionValue, $currentOptionValue);
                    } else {
                        $bSelected = $currentOptionValue == $strOptionValue;
                    }
                    $bSelectedExists = $bSelectedExists || $bSelected;
                    ?>
                    <option value="<?=$strOptionValue?>" <?=$bSelected ? ' selected' : ''?>><?=$strOptionName?></option>
                    <?
                }
                $strOptions = ob_get_clean();
                if ($arProperty['IS_REQUIRED'] != 'Y' || $arProperty['MULTIPLE'] == 'Y') {
                    ?>
                    <option value="" <?=!$bSelectedExists ? ' selected' : ''?>>(Не установлено)</option>
                    <?
                }
                echo $strOptions;
            ?>
            </select>
            <?
        } elseif ($arProperty['LIST_TYPE'] == 'C') {
            $bHideLabel = count($arProperty['VALUES']) == 1;
            $strInputType = ($arProperty['MULTIPLE'] == 'Y' || count($arProperty['VALUES']) == 1) ? 'checkbox' : 'radio';
            foreach ($arProperty['VALUES'] as $strKey => $strOptionValue) {
                $bChecked = $currentOptionValue == $strKey || (is_array($currentOptionValue) && in_array($strKey, $currentOptionValue));
                ?>
                <div>
                <?
                    if ($strInputType == 'checkbox') {
                        ?>
                        <input<?=$strControlName?> id="<?=$arProperty['ID'].'_'.$strKey?>" type="hidden" value="" checked/>
                        <?
                    }
                    ?><input<?=$strControlName?> id="<?=$arProperty['ID'].'_'.$strKey?>" type="<?=$strInputType?>" value="<?=$strKey?>"<?=$bChecked ? ' checked' : ''?>/><?
                    if (!$bHideLabel) {
                        ?><label for="<?=$arProperty['ID'].'_'.$strKey?>"><?=$strOptionValue?></label><?
                    }
                ?>
                </div>
                <?
            }
        }
    }

    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
        $arProperty['VALUES'] = static::getList();
        ob_start();
        static::showSelect($arProperty, $value, $strHTMLControlName);
        return ob_get_clean();
    }

    public static function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
    {
        $arPropertyFields = array(
            "HIDE" => array("SEARCHABLE", "WITH_DESCRIPTION", "ROW_COUNT", "COL_COUNT", "DEFAULT_VALUE", "HINT", "SMART_FILTER"),
        );
        return '';
    }

    public static function GetAdminListViewHTML($arProperty, $arValue, $strHTMLControlName)
    {
        return static::getValueName($arValue['VALUE']);
    }
}
