<?php

namespace Rapid\UserTypes\UserType;

class UserEnum extends \CDBResult
{
    function GetList($ACTIVE_FILTER = 'N')
    {
        $arFilter = array();
        if ($ACTIVE_FILTER === 'Y') {
            $arFilter['ACTIVE'] = 'Y';
        }

        $by = 'name';
        $order = 'asc';
        $rs = \CUser::GetList($by, $order, $arFilter, array('FIELDS' => array('ID', 'NAME', 'LAST_NAME', 'LOGIN')));
        if ($rs) {
            $rs = new self($rs);
        }

        return $rs;
    }

    function GetNext($bTextHtmlAuto = true, $use_tilda = true)
    {
        $ar = parent::GetNext($bTextHtmlAuto, $use_tilda);
        if ($ar) {
            $ar['VALUE'] = '[' . $ar['LOGIN'] . ']' . $ar['NAME'] . ' ' . $ar['LAST_NAME'];
        }

        return $ar;
    }
}
