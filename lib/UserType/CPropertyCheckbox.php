<?

namespace Rapid\UserTypes\UserType;

class CPropertyCheckbox
{
    protected static $arVals = [
        'FALSE' => 0,
        'TRUE' => 1,
    ];

    protected static function val($valCode)
    {
        return static::$arVals[$valCode];
    }

    protected static function checkVal($value)
    {
        return (int)$value == static::val('TRUE') ? static::val('TRUE') : static::val('FALSE');
    }

    public static function GetUserTypeDescription()
    {
        return array(
            'PROPERTY_TYPE' => 'N',
            'USER_TYPE' => 'Checkbox',
            'DESCRIPTION' => 'R: Чекбокс',
            'ConvertToDB' => array(__CLASS__, 'ConvertToDB'),
            'ConvertFromDB' => array(__CLASS__, 'ConvertFromDB'),
            'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
            'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
            'GetPublicViewHTML' => array(__CLASS__, 'GetPublicViewHTML'),
            'GetPublicEditHTML' => array(__CLASS__, 'GetPublicEditHTML'),
            'GetAdminFilterHTML' => array(__CLASS__, 'GetAdminFilterHTML'),
            'GetPublicFilterHTML' => array(__CLASS__, 'GetPublicFilterHTML'),
            'GetSettingsHTML' => array(__CLASS__, 'GetSettingsHTML'),
            'PrepareSettings' => array(__CLASS__, 'PrepareSettings'),
        );
    }

    public static function ConvertToDB($arProperty, $value)
    {
        $value['VALUE'] = static::checkVal($value['VALUE']);
        return $value;
    }

    public static function ConvertFromDB($arProperty, $value)
    {
        if ($value['VALUE'] != '') {
            $value['VALUE'] = static::checkVal($value['VALUE']);
        }
        return $value;
    }

    public static function GetSettingsHTML($arFields, $strHTMLControlName, &$arPropertyFields)
    {
        $arPropertyFields = array(
            'HIDE' => array('ROW_COUNT', 'COL_COUNT', 'MULTIPLE_CNT', 'WITH_DESCRIPTION'),
            'USER_TYPE_SETTINGS_TITLE' => 'Настройки показа',
        );

        $arSettings = self::PrepareSettings($arFields);

        ob_start();
        ?>
        <tr>
        <td>Текст для пустого чекбокса</td>
        <td><input type="text"
                   name="<?=$strHTMLControlName['NAME']?>[VIEW][<?=static::val('FALSE')?>]"
                   value="<?=htmlspecialcharsbx($arSettings['VIEW'][static::val('FALSE')])?>"></td>
        </tr>
        <tr>
        <td>Текст для нажатого чекбокса</td>
        <td><input type="text"
                   name="<?=$strHTMLControlName['NAME']?>[VIEW][<?=static::val('TRUE')?>]"
                   value="<?=htmlspecialcharsbx($arSettings['VIEW'][static::val('TRUE')])?>"></td>
        </tr><?php
        $strResult = ob_get_contents();
        ob_end_clean();

        return $strResult;
    }

    public static function GetPropertyFieldHtml($arProperty, $arValue, $strHTMLControlName)
    {
        if ($arValue['VALUE'] === null || $arValue['VALUE'] === false || $arValue['VALUE'] === '') {
            $newID = (!isset($_REQUEST['ID']) || (int)$_REQUEST['ID'] <= 0 || (isset($_REQUEST['action']) && $_REQUEST['action'] == 'copy'));
            if ($newID) {
                $arValue['VALUE'] = $arProperty['DEFAULT_VALUE'];
            } else {
                $arValue['VALUE'] = static::val('FALSE');
            }
            unset($index);
        }
        $arValue['VALUE'] = static::checkVal($arValue['VALUE']);
        ob_start();
        ?>
        <input type="hidden" name="<?=htmlspecialcharsbx($strHTMLControlName['VALUE'])?>"
               id="<?=$strHTMLControlName['VALUE']?>_N" value="<?=static::val('FALSE')?>">
        <input type="checkbox" name="<?=htmlspecialcharsbx($strHTMLControlName['VALUE'])?>"
            id="<?=$strHTMLControlName['VALUE']?>_Y" value="<?=static::val('TRUE')?>"<?=($arValue['VALUE'] == static::val('TRUE') ? ' checked="checked"' : '')?>/>
        <?
        return ob_get_clean();
    }

    public static function GetAdminListViewHTML($arProperty, $arValue, $strHTMLControlName)
    {
        $arSettings = static::PrepareSettings($arProperty);
        if ('|' . $arValue['VALUE'] != '|' . (int)$arValue['VALUE']) {
            return 'пусто';
        }
        if ($arValue['VALUE'] != static::val('TRUE') && $arValue['VALUE'] != static::val('FALSE')) {
            return 'пусто';
        }
        return htmlspecialcharsex($arSettings['VIEW'][$arValue['VALUE']]);
    }

    public static function GetAdminFilterHTML($arProperty, $strHTMLControlName)
    {

        $arSettings = static::PrepareSettings($arProperty);

        $strCurValue = '';
        if (array_key_exists($strHTMLControlName['VALUE'],
                $_REQUEST) && ($_REQUEST[$strHTMLControlName['VALUE']] == static::val('TRUE') || $_REQUEST[$strHTMLControlName['VALUE']] == static::val('FALSE'))) {
            $strCurValue = $_REQUEST[$strHTMLControlName['VALUE']];
        } elseif (isset($GLOBALS[$strHTMLControlName['VALUE']]) && ($GLOBALS[$strHTMLControlName['VALUE']] == static::val('TRUE') || $GLOBALS[$strHTMLControlName['VALUE']] == static::val('FALSE'))) {
            $strCurValue = $GLOBALS[$strHTMLControlName['VALUE']];
        }

        $strResult = '<select name="' . htmlspecialcharsbx($strHTMLControlName['VALUE']) . '" id="filter_' . htmlspecialcharsbx($strHTMLControlName['VALUE']) . '">';
        $strResult .= '<option value=""' . ('' == $strCurValue ? ' selected="selected"' : '') . '>любое</option>';
        foreach ($arSettings['VIEW'] as $key => $value) {
            $strResult .= '<option value="' . intval($key) . '"' . ($strCurValue != '' && $key == $strCurValue ? ' selected="selected"' : '') . '>' . htmlspecialcharsex($value) . '</option>';
        }
        $strResult .= '</select>';

        return $strResult;
    }

    public static function GetPublicViewHTML($arProperty, $arValue, $strHTMLControlName)
    {
        $arSettings = static::PrepareSettings($arProperty);
        if ('|' . $arValue['VALUE'] != '|' . (int)$arValue['VALUE']) {
            return 'пусто';
        }
        if ($arValue['VALUE'] != static::val('TRUE') && $arValue['VALUE'] != static::val('FALSE')) {
            return 'пусто';
        }
        return htmlspecialcharsex($arSettings['VIEW'][$arValue['VALUE']]);
    }

    public static function GetPublicEditHtml($arProperty, $arValue, $strHTMLControlName)
    {
        if ($arValue['VALUE'] === null || $arValue['VALUE'] === false || $arValue['VALUE'] === '') {
            $arValue['VALUE'] = $arProperty['DEFAULT_VALUE'];
        }

        $arValue['VALUE'] = static::val($arValue['VALUE']);

        $strResult = '<input type="hidden" name="' . htmlspecialcharsbx($strHTMLControlName['VALUE']) . '" id="' . $strHTMLControlName['VALUE'] . '_N" value="' . static::val('FALSE') . '" />' .
            '<input type="checkbox" name="' . htmlspecialcharsbx($strHTMLControlName['VALUE']) . '" id="' . $strHTMLControlName['VALUE'] . '_Y" value="' . static::val('TRUE') . '" ' . ($arValue['VALUE'] == static::val('TRUE') ? 'checked="checked"' : '') . '/>';
        return $strResult;
    }

    public static function GetPublicFilterHTML($arProperty, $strHTMLControlName)
    {
        $arSettings = static::PrepareSettings($arProperty);

        $strCurValue = '';
        if (isset($_REQUEST[$strHTMLControlName['VALUE']]) && ($_REQUEST[$strHTMLControlName['VALUE']] == static::val('TRUE') || $_REQUEST[$strHTMLControlName['VALUE']] == static::val('FALSE'))) {
            $strCurValue = $_REQUEST[$strHTMLControlName['VALUE']];
        } elseif (
            isset($strHTMLControlName['GRID_ID'])
            && isset($_SESSION['main.interface.grid'][$strHTMLControlName['GRID_ID']]['filter'][$strHTMLControlName['VALUE']])
        ) {
            $strCurValue = $_SESSION['main.interface.grid'][$strHTMLControlName['GRID_ID']]['filter'][$strHTMLControlName['VALUE']];
        }

        $strResult = '<select name="' . htmlspecialcharsbx($strHTMLControlName['VALUE']) . '" id="filter_' . htmlspecialcharsbx($strHTMLControlName['VALUE']) . '">';
        $strResult .= '<option value=""' . ('' == $strCurValue ? ' selected="selected"' : '') . '>любое</option>';
        foreach ($arSettings['VIEW'] as $key => $value) {
            $strResult .= '<option value="' . intval($key) . '"' . ($strCurValue != '' && $key == $strCurValue ? ' selected="selected"' : '') . '>' . htmlspecialcharsex($value) . '</option>';
        }
        $strResult .= '</select>';

        return $strResult;
    }

    public static function PrepareSettings($arFields)
    {
        $arDefView = self::GetDefaultListValues();
        $arView = array();
        if (
            array_key_exists('USER_TYPE_SETTINGS', $arFields) && is_array($arFields['USER_TYPE_SETTINGS']) &&
            array_key_exists('VIEW', $arFields['USER_TYPE_SETTINGS']) &&
            !empty($arFields['USER_TYPE_SETTINGS']['VIEW']) && is_array($arFields['USER_TYPE_SETTINGS']['VIEW'])
        ) {
            $arView = $arFields['USER_TYPE_SETTINGS']['VIEW'];
        }

        if (empty($arView)) {
            $arView = $arDefView;
        }

        return array(
            'VIEW' => $arView
        );
    }

    protected static function GetDefaultListValues()
    {
        return array(
            static::val('FALSE') => 'Нет',
            static::val('TRUE') => 'Да',
        );
    }
}
