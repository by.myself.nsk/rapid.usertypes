<?php
namespace Rapid\UserTypes\UserType;
use \CIBlock;

class CUserFieldIBlockProperties extends \CUserTypeEnum
{
    private const MODE_EDIT_FORM = 'main.edit_form';
    public static function GetUserTypeDescription()
    {
        return array(
            'USER_TYPE_ID' => 'iblock_properties',
            'CLASS_NAME' => static::class,
            'DESCRIPTION' => 'R: Привязка к свойствам инфоблока',
            'BASE_TYPE' => 'enum',
        );
    }
    public function prepareSettings(array $userField): array
    {
        $iblock_id = (int)$userField['SETTINGS']['IBLOCK_ID'];
        if ($iblock_id <= 0) {
            $iblock_id = '';
        }
        return [
            'IBLOCK_ID' => $iblock_id,
        ];
    }
    function getSettingsHtml($userField, $additionalParameters, $varsFromForm)
    {
        $userField['USE_COMPONENT'] = 'Y';
        global $APPLICATION;
        ob_start(); ?>
        <tr>
            <td>
                <span class="adm-detail-label-text">Инфоблок :</span>
            </td>
            <td>
                <?=
                GetIBlockDropDownList(
                    $userField['SETTINGS']['IBLOCK_ID'],
                    $additionalParameters['NAME'] . '[IBLOCK_TYPE_ID]',
                    $additionalParameters['NAME'] . '[IBLOCK_ID]',
                    false,
                    'class="adm-detail-iblock-types"',
                    'class="adm-detail-iblock-list" onchange="showUsertypeElementNote(this);"'
                ) ?>
            </td>
        </tr>
        <?php
        return ob_get_clean();
    }
    public function getEditFormHtml(array $userField, ?array $additionalParameters): string
    {
        return self::getEditFormHtmlMulty($userField, $additionalParameters);
    }
    public function getEditFormHtmlMulty(array $userField, ?array $additionalParameters): string
    {
        $additionalParameters['items'] = self::getListProps($userField);
        $additionalParameters['mode'] = self::MODE_EDIT_FORM;
        $userField['USE_COMPONENT'] = 'Y';
        $userField['SETTINGS']['DISPLAY'] = 'LIST';
        $userField['SETTINGS']['LIST_HEIGHT'] = '10';
        global $APPLICATION;
        ob_start();
        $APPLICATION->IncludeComponent(
            'bitrix:main.field.enum',
            '',
            [
                'userField' => $userField,
                'additionalParameters' => $additionalParameters,
            ]
        );
        return ob_get_clean();
    }
    public function getListProps(array $userField)
    {
        $IBLOCK_ID = $userField['SETTINGS']['IBLOCK_ID'];
        $propsRes = CIBlock::GetProperties($IBLOCK_ID);
        $props = [];
        while ($prop = $propsRes->Fetch()) {
            $props[$prop["ID"]] = [
                "ID" => $prop["ID"],
                "VALUE" => $prop['NAME'] . ' [' . $prop["CODE"] . ']'
            ];
        }
        return $props;
    }
}
