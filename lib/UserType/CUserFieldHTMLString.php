<?php

namespace Rapid\UserTypes\UserType;

/**
 * Пользовательское поле "Строка с HTML редактором"
 * Class CUserTypeHTMLString
 */
class CUserFieldHTMLString extends \CUserTypeString
{
    public static function GetUserTypeDescription()
    {
        return array(
            'USER_TYPE_ID' => 'html_string',
            'CLASS_NAME'   => static::class,
            'DESCRIPTION'  => 'R: Строка с HTML редактором',
            'BASE_TYPE'    => 'string',
        );
    }

    public function GetEditFormHTML($arUserField, $arHtmlControl)
    {
        if (!\CModule::IncludeModule('fileman')) {
            return GetMessage('IBLOCK_PROP_HTML_NOFILEMAN_ERROR');
        }

        if ($arUserField["ENTITY_VALUE_ID"] < 1 && strlen($arUserField["SETTINGS"]["DEFAULT_VALUE"]) > 0) {
            $arHtmlControl["VALUE"] = htmlspecialcharsbx($arUserField["SETTINGS"]["DEFAULT_VALUE"]);
        }
        if ($arUserField["SETTINGS"]["ROWS"] < 8) {
            $arUserField["SETTINGS"]["ROWS"] = 8;
        }

        if ($arUserField['MULTIPLE'] == 'Y') {
            $name = preg_replace("/[\[\]]/i", "_", $arHtmlControl["NAME"]);
        } else {
            $name = $arHtmlControl["NAME"];
        }

        ob_start();
        \CFileMan::AddHTMLEditorFrame(
            $name,
            $arHtmlControl['VALUE'],
            $name . '_TYPE',
            'html',
            array(
                'height' => 450,
                'width'  => '100%'
            )
        );

        if ($arUserField['MULTIPLE'] == 'Y') {
            echo '<input type="hidden" name="' . $arHtmlControl["NAME"] . '" >';
        }

        return ob_get_clean();
    }

    public static function OnBeforeSave($arUserField, $value)
    {
        if ($arUserField['MULTIPLE'] == 'Y') {
            foreach ($_POST as $key => $val) {
                if (preg_match("/" . $arUserField['FIELD_NAME'] . "_([0-9]+)_$/i", $key, $m)) {
                    $value = $val;
                    unset($_POST[$key]);
                    break;
                }
            }
        }

        return $value;
    }
}
