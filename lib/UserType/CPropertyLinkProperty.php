<?php

namespace Rapid\UserTypes\UserType;

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class CPropertyLinkProperty
{
    public static function GetUserTypeDescription()
    {
        return array(
            "PROPERTY_TYPE" => 'E',
            "USER_TYPE" => 'LinkProperty',
            'DESCRIPTION' => 'R: Привязка к свойствам инфоблока',
            "GetPropertyFieldHtml" => array(self::class, "GetPropertyFieldHtml"),
            "GetPublicViewHTML" => array(self::class, "GetPublicViewHTML"),
            "GetAdminListViewHTML" => array(self::class, "GetAdminListViewHTML"),
        );
    }

    public static function getList($IBLOCK_ID)
    {
        $arItems = [];
        $dbItems = \CIBlock::GetProperties($IBLOCK_ID);

        while ($arItem = $dbItems->Fetch()) {
            $arItems[$arItem['CODE']] = $arItem['NAME'] . ' [' . $arItem['CODE'] . ']';
        }

        return $arItems;
    }

    protected static function getValueName($intValId, $IBLOCK_ID)
    {
        $arValues = static::getList($IBLOCK_ID);
        return $arValues[$intValId];
    }

    protected static function showSelect($arProperty, $value, $strHTMLControlName)
    {
        if (($intSize = intval($arProperty["USER_TYPE_SETTINGS"]["size"])) > 1) {
            $size = ' size="' . $intSize . '"';
        } else {
            $size = '';
        }
        $currentOptionValue = $value['VALUE'];

        $strControlName = ' name="' . $strHTMLControlName["VALUE"] . '"';

        if ($arProperty['LIST_TYPE'] == 'L') {
            ?>
            <select<?= $strControlName ?><?= $size ?> style="margin-bottom: 6px;">
                <?
                $bSelectedExists = false;
                ob_start();
                foreach ($arProperty['VALUES'] as $strOptionValue => $strOptionName) {
                    if ($arProperty['MULTIPLE'] == 'Y') {
                        if (!is_array($currentOptionValue)) {
                            $currentOptionValue = array($currentOptionValue);
                        }
                        $bSelected = in_array($strOptionValue, $currentOptionValue);
                    } else {
                        $bSelected = $currentOptionValue == $strOptionValue;
                    }
                    $bSelectedExists = $bSelectedExists || $bSelected;
                    ?>
                    <option value="<?= $strOptionValue ?>" <?= $bSelected ? ' selected' : '' ?>><?= $strOptionName ?></option>
                    <?
                }
                $strOptions = ob_get_clean();
                if ($arProperty['IS_REQUIRED'] != 'Y' || $arProperty['MULTIPLE'] == 'Y') {
                    ?>
                    <option value="" <?= !$bSelectedExists ? ' selected' : '' ?>>(Не установлено)</option>
                    <?
                }
                echo $strOptions;
                ?>
            </select>
            <?
        } elseif ($arProperty['LIST_TYPE'] == 'C') {
            $bHideLabel = count($arProperty['VALUES']) == 1;
            $strInputType = ($arProperty['MULTIPLE'] == 'Y' || count(
                    $arProperty['VALUES']
                ) == 1) ? 'checkbox' : 'radio';
            foreach ($arProperty['VALUES'] as $strKey => $strOptionValue) {
                $bChecked = $currentOptionValue == $strKey || (is_array($currentOptionValue) && in_array(
                            $strKey,
                            $currentOptionValue
                        ));
                ?>
                <div>
                    <?
                    if ($strInputType == 'checkbox') {
                        ?>
                        <input<?= $strControlName ?> id="<?= $arProperty['ID'] . '_' . $strKey ?>" type="hidden"
                                                     value="" checked/>
                        <?
                    }
                    ?><input<?= $strControlName ?> id="<?= $arProperty['ID'] . '_' . $strKey ?>"
                                                   type="<?= $strInputType ?>"
                                                   value="<?= $strKey ?>"<?= $bChecked ? ' checked' : '' ?>/><?
                    if (!$bHideLabel) {
                        ?><label for="<?= $arProperty['ID'] . '_' . $strKey ?>"><?= $strOptionValue ?></label><?
                    }
                    ?>
                </div>
                <?
            }
        }
    }

    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
        $arProperty['VALUES'] = static::getList($arProperty['LINK_IBLOCK_ID']);
        ob_start();
        static::showSelect($arProperty, $value, $strHTMLControlName);
        return ob_get_clean();
    }

    public static function GetAdminListViewHTML($arProperty, $arValue, $strHTMLControlName)
    {
        return static::getValueName($arValue['VALUE'], $arProperty['LINK_IBLOCK_ID']);
    }

    public static function isIblockIncluded(): bool
    {
        return Loader::includeModule('iblock');
    }
}
