<?php

namespace Rapid\UserTypes\UserType;

class CUserFieldUserId extends \CUserTypeEnum
{
    public static function GetUserTypeDescription()
    {
        return array(
            'USER_TYPE_ID' => 'user_id',
            'CLASS_NAME'   => static::class,
            'DESCRIPTION'  => 'R: Привязка к пользователю',
            'BASE_TYPE'    => 'int',
        );
    }

    function PrepareSettings($arUserField)
    {
        $height = intval($arUserField['SETTINGS']['LIST_HEIGHT']);
        $disp = $arUserField['SETTINGS']['DISPLAY'];
        if ($disp != 'CHECKBOX' && $disp != 'LIST') {
            $disp = 'LIST';
        }

        $active_filter = $arUserField['SETTINGS']['ACTIVE_FILTER'] === 'Y' ? 'Y' : 'N';

        return array(
            'DISPLAY'       => $disp,
            'LIST_HEIGHT'   => ($height < 1 ? 1 : $height),
            'DEFAULT_VALUE' => '',
            'ACTIVE_FILTER' => $active_filter,
        );
    }

    function GetSettingsHTML($arUserField = false, $arHtmlControl, $bVarsFromForm)
    {
        $result = '';

        if ($bVarsFromForm) {
            $value = $GLOBALS[$arHtmlControl['NAME']]['DISPLAY'];
        } elseif (is_array($arUserField)) {
            $value = $arUserField['SETTINGS']['DISPLAY'];
        } else {
            $value = 'LIST';
        }
        $result .= '
		<tr>
			<td class="adm-detail-valign-top">Внешний вид:</td>
			<td>
				<label><input type="radio" name="' . $arHtmlControl['NAME'] . '[DISPLAY]" value="LIST" ' . ('LIST' == $value ? 'checked' : '') . '>Список</label><br>
				<label><input type="radio" name="' . $arHtmlControl['NAME'] . '[DISPLAY]" value="CHECKBOX" ' . ('CHECKBOX' == $value ? 'checked' : '') . '>Флажки</label><br>
			</td>
		</tr>
		';

        if ($bVarsFromForm) {
            $value = intval($GLOBALS[$arHtmlControl['NAME']]['LIST_HEIGHT']);
        } elseif (is_array($arUserField)) {
            $value = intval($arUserField['SETTINGS']['LIST_HEIGHT']);
        } else {
            $value = 1;
        }
        $result .= '
		<tr>
			<td>Высота списка:</td>
			<td>
				<input type="text" name="' . $arHtmlControl['NAME'] . '[LIST_HEIGHT]" size="10" value="' . $value . '">
			</td>
		</tr>
		';

        if ($bVarsFromForm) {
            $ACTIVE_FILTER = $GLOBALS[$arHtmlControl['NAME']]['ACTIVE_FILTER'] === 'Y' ? 'Y' : 'N';
        } elseif (is_array($arUserField)) {
            $ACTIVE_FILTER = $arUserField['SETTINGS']['ACTIVE_FILTER'] === 'Y' ? 'Y' : 'N';
        } else {
            $ACTIVE_FILTER = 'N';
        }

        $result .= '
		<tr>
			<td>Показывать только активных пользователей:</td>
			<td>
				<input type="checkbox" name="' . $arHtmlControl['NAME'] . '[ACTIVE_FILTER]" value="Y" ' . ($ACTIVE_FILTER == 'Y' ? 'checked' : '') . '>
			</td>
		</tr>
		';

        return $result;
    }

    public static function GetList($arUserField)
    {
        $obUser = new \Rapid\UserType\UserEnum();

        return $obUser->GetList($arUserField['SETTINGS']['ACTIVE_FILTER']);
    }

    function OnSearchIndex($arUserField)
    {
        $res = '';

        if (is_array($arUserField['VALUE'])) {
            $val = $arUserField['VALUE'];
        } else {
            $val = array($arUserField['VALUE']);
        }

        $val = array_filter($val, 'strlen');
        if (count($val)) {
            $ob = new \CUser();
            $rs = $ob->GetList($by, $order, array(
                '=ID' => $val
            ), array('FIELDS' => array('ID', 'NAME', 'LAST_NAME')));

            while ($ar = $rs->Fetch()) {
                $res .= '[' . $ar['LOGIN'] . ']' . $ar['NAME'] . ' ' . $ar['LAST_NAME'] . "\r\n";
            }
        }

        return $res;
    }
}
