<?

namespace Rapid\UserTypes\UserType;

class CPropertyLinkButton
{
    protected static $inputSize = 30;

    public function GetUserTypeDescription()
    {
        return [
            'PROPERTY_TYPE' => 'S',
            'USER_TYPE' => 'link',
            'DESCRIPTION' => 'R: Ссылка/Кнопка',
            'ConvertToDB' => [__CLASS__, 'ConvertToDB'],
            'ConvertFromDB' => [__CLASS__, 'ConvertFromDB'],
            'GetPropertyFieldHtml' => [__CLASS__, 'GetPropertyFieldHtml'],
            'GetAdminListViewHTML' => [__CLASS__, 'GetAdminListViewHTML'],
            'GetPublicViewHTML' => [__CLASS__, 'GetPublicViewHTML'],
            'GetPublicEditHTML' => [__CLASS__, 'GetPublicEditHTML'],
            'GetSettingsHTML' => [__CLASS__, 'GetSettingsHTML'],
            'PrepareSettings' => [__CLASS__, 'PrepareSettings'],
        ];
    }

    /**
     * Получаем параметры свойства
     * @param $arProperty
     * @return array
     */
    protected static function getPropertySettings($arProperty)
    {
        static $arSettings = [];

        $propertyId = $arProperty['ID'];

        if ($arSettings[$propertyId]) {
            return $arSettings[$propertyId];
        }

        $arPropertyFields = self::getPropertyFields();
        foreach ($arPropertyFields as $fieldCode => $fieldData) {
            if (!$fieldData['SETTINGS']['SHOW_VISIBILITY']) {
                continue;
            }
            $settingCode = 'show_' . $fieldCode;
            $defaultValue = $fieldData['SETTINGS']['DEFAULT_VISIBLE'] ? 'Y' : 'N';
            $arSettings[$propertyId][$settingCode] = [
                'CODE' => $settingCode,
                'NAME' => 'Показывать контрол "' . $fieldData['NAME'] . '"',
                'TYPE' => 'CHECKBOX',
                'DEFAULT_VALUE' => $defaultValue,
                'VALUE' => $arProperty['USER_TYPE_SETTINGS'][$settingCode] ?? $defaultValue,
            ];
        }
        return $arSettings[$propertyId];
    }

    /**
     * Получаем контролы свойства
     * @param $arValues
     * @return array[]
     */
    protected static function getPropertyFields($arValues = [])
    {
        $arFields = [
            'title' => [
                'CODE' => 'title',
                'NAME' => 'Текст',
                'TYPE' => 'TEXT',
                'DEFAULT_VALUE' => '',
                'SETTINGS' => [
                    'SHOW_VISIBILITY' => true,
                    'DEFAULT_VISIBLE' => true,
                ]
            ],
            'link' => [
                'CODE' => 'link',
                'NAME' => 'Ссылка',
                'TYPE' => 'TEXT',
                'DEFAULT_VALUE' => '',
                'SETTINGS' => [
                    'SHOW_VISIBILITY' => true,
                    'DEFAULT_VISIBLE' => true,
                ]
            ],
            'is_blank' => [
                'CODE' => 'is_blank',
                'NAME' => 'Открывать в новом окне',
                'TYPE' => 'CHECKBOX',
                'DEFAULT_VALUE' => 'N',
                'SETTINGS' => [
                    'SHOW_VISIBILITY' => true,
                    'DEFAULT_VISIBLE' => true,
                ]
            ],
        ];
        foreach ($arFields as $code => &$data) {
            $data['VALUE'] = $arValues[$code];
            $data['VALUE_ESCAPED'] = htmlspecialchars($data['VALUE']);
        }
        unset($data);
        return $arFields;
    }

    protected static function getPropertySettingValue($arProperty, $settingCode)
    {
        $settings = self::getPropertySettings($arProperty);
        $settingValue = $settings[$settingCode]['VALUE'];
        return $settingValue;
    }

    protected static function checkValueForValidity(&$value)
    {
        if (!is_array($value)) {
            $value = [];
        }

        $arPropertyFields = self::getPropertyFields();

        foreach ($arPropertyFields as $code => $arPropertyField) {
            self::checkValueKeyExists($value, $code, $arPropertyField['DEFAULT_VALUE']);
        }
        return $value;
    }

    protected static function checkValueKeyExists(&$value, $key, $defaultValue)
    {
        if (!array_key_exists($key, $value)) {
            $value[$key] = $defaultValue;
        }
    }

    protected static function showLink($value)
    {
        ?><a href="<?=$value['link']?>"<?=$value['is_blank'] ? 'target="_blank"' : ''?><?=$value['additional_attributes']?>><?=$value['title']?></a><?
    }

    public function ConvertToDB($arProp, $val)
    {
        self::checkValueForValidity($val['VALUE']);
        $val['VALUE'] = json_encode($val['VALUE']);
        return $val;
    }

    public function ConvertFromDB($arProp, $val)
    {
        $val['VALUE'] = json_decode($val['VALUE'], 1);
        self::checkValueForValidity($val['VALUE']);
        return $val;
    }

    public static function GetPropertyFieldHtml($arProperty, $arValue, $arHTMLControlNames)
    {
        $settings = self::getPropertySettings($arProperty);
        $fields = self::getPropertyFields($arValue['VALUE']);
        $formMode = $arHTMLControlNames['MODE'];
        // EDIT_FORM - окно параметров свойства
        // FORM_FILL - в форме редактирования элемента
        // iblock_element_admin - редактирование в режиме просмотра списка элементов
        $isPropertyParametersForm = $formMode == 'EDIT_FORM';
        ob_start();
        ?>
        <table>
            <?
            foreach ($fields as $code => $field) {
                $showCode = 'show_' . $code;
                $arShowSetting = $settings[$showCode];
                $controlName = $arHTMLControlNames['VALUE'] . '[' . $code . ']';

                if (!$isPropertyParametersForm && ($arShowSetting['VALUE'] == 'N')) {
                    ?><tr>
                        <td></td>
                        <td><input type="hidden" name="<?=$controlName?>" value="<?=$field['VALUE_ESCAPED']?>"></td>
                    </tr>
                    <?
                    continue;
                }
                ?><tr>
                    <td><?=$field['NAME']?></td>
                    <td>
                        <?
                        switch ($field['TYPE']) {
                            case 'HIDDEN':
                                ?><input type="hidden" name="<?=$controlName?>" value="<?=$field['VALUE_ESCAPED']?>"><?
                                break;
                            case 'TEXT':
                                ?>
                                <input type="text" size="<?=static::$inputSize?>"
                                        name="<?=$controlName?>"
                                        value="<?=$field['VALUE_ESCAPED']?>">
                                <?
                                break;
                            case 'CHECKBOX':
                                ?>
                                <input type="hidden" name="<?=$controlName?>" value="0">
                                <input type="checkbox" size="<?=static::$inputSize?>"
                                        name="<?=$controlName?>"
                                        value="1"<?=$arValue['VALUE']['is_blank'] ? ' checked' : ''?>>
                                <?
                                break;
                        }
                        ?>
                    </td>
                </tr>
                <?
            }
            ?>
        </table>
        <?
        return ob_get_clean();
    }

    public static function GetAdminListViewHTML($arProperty, $arValue, $arHTMLControlNames)
    {
        ob_start();
        self::showLink($arValue['VALUE']);
        return ob_get_clean();
    }

    public static function GetPublicViewHTML($arProperty, $arValue, $arHTMLControlNames)
    {
        return self::GetAdminListViewHTML($arProperty, $arValue, $arHTMLControlNames);
    }

    public static function GetPublicEditHTML($arProperty, $arValue, $arHTMLControlNames)
    {
        return self::GetPropertyFieldHtml($arProperty, $arValue, $arHTMLControlNames);
    }

    /**
     * Метод для показа параметров свойства в попапе
     * @param $arProperty
     * @param $arHTMLControlNames
     * @param $arPropertyFields
     * @return false|string
     */
    public static function GetSettingsHTML($arProperty, $arHTMLControlNames, &$arPropertyFields)
    {
        $arPropertyFields = [
            'HIDE' => ['SEARCHABLE', 'FILTRABLE', 'WITH_DESCRIPTION', 'ROW_COUNT', 'COL_COUNT',],
            'SET' => [
                'SEARCHABLE' => 'N',
                'FILTRABLE' => 'N',
                'WITH_DESCRIPTION' => 'N',
                'DEFAULT_VALUE' => []
            ],
            'USER_TYPE_SETTINGS_TITLE' => 'Настройки показа',
        ];

        $settings = self::getPropertySettings($arProperty);
        ob_start();

        foreach ($settings as $settingCode => $settingData) {
            $settingValue = self::getPropertySettingValue($arProperty, $settingCode);
            ?>
            <tr>
                <td><?=$settingData['NAME']?></td>
                <td>
                    <?
                    switch ($settingData['TYPE']) {
                        case 'TEXT':
                            ?><input type="text" size="5" name="<?=$arHTMLControlNames["NAME"]?>[<?=$settingCode?>]" value="<?=$settingValue?>">
                            <?
                            break;
                        case 'CHECKBOX':
                            ?>
                            <input type="hidden" id="<?=$settingCode?>_N" name="<?=$arHTMLControlNames["NAME"]?>[<?=$settingCode?>]" value="N" checked>
                            <input type="checkbox" id="<?=$settingCode?>_Y" name="<?=$arHTMLControlNames["NAME"]?>[<?=$settingCode?>]" value="Y"
                                    class="adm-designed-checkbox"<?=$settingValue == 'Y' ? ' checked' : ''?>
                            >
                            <label class="adm-designed-checkbox-label" for="<?=$settingCode?>_Y" title=""></label>
                            <?
                            break;
                    }
                    ?>
                </td>
            </tr>
            <?
        }
        $strResult = ob_get_clean();
        return $strResult;
    }

    /**
     * Метод для сохранения параметров свойства в БД при сохранении свойства
     * @param $arProperty
     * @return mixed
     */
    public static function PrepareSettings($arProperty)
    {
        $values = $arProperty['USER_TYPE_SETTINGS'];
        $settings = self::getPropertySettings($arProperty);
        foreach ($settings as $settingCode => $settingData) {
            $settingValue = self::getPropertySettingValue($arProperty, $settingCode);
            $values[$settingCode] = $settingValue;
        }
        return $values;
    }
}
