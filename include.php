<?php

use Bitrix\Main\Loader;
use Rapid\UserTypes\UserTypes;

Loader::registerNamespace("Rapid\\UserTypes", __DIR__ . '/lib');

UserTypes::addEventHandlers();
