<?php
if (!check_bitrix_sessid()) {
    return;
}
global $APPLICATION, $errors;
if ($errors === false) {
    echo CAdminMessage::ShowNote(GetMessage("MOD_INST_OK"));
} else {
    echo CAdminMessage::ShowMessage(array("TYPE" => "ERROR", "MESSAGE" => GetMessage("MOD_INST_ERR"), "DETAILS" => $errors, "HTML" => true));
}
?>
<form action="<?=$APPLICATION->GetCurPage()?>">
    <input type="hidden" name="lang" value="<?=LANGUAGE_ID?>">
    <input type="submit" name="" value="<?=GetMessage("MOD_BACK")?>">
    <form>
