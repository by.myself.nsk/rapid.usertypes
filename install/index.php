<?

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;


Class Rapid_Usertypes extends CModule
{
    public function __construct()
    {
        Loc::loadMessages(__FILE__);

        $this->fillModuleFields();
    }

    private function fillModuleFields()
    {
        $this->path = $this->getModulePath();
        $this->MODULE_ID = $this->getModuleIdFromModulePath($this->path);
        $this->MODULE_NAME = GetMessage('RAPID_USERTYPES_MODULE_NAME');
        $this->MODULE_DESCRIPTION = GetMessage('RAPID_USERTYPES_MODULE_DESCRIPTION');
        $this->PARTNER_NAME = GetMessage('RAPID_USERTYPES_PARTNER_NAME');
        $this->PARTNER_URI = GetMessage('RAPID_USERTYPES_PARTNER_URI');
        $arModuleVersion = [];
        /** @noinspection PhpIncludeInspection */
        include($this->path . '/version.php');
        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }
    }

    private function getModulePath()
    {
        return dirname(__FILE__);
    }

    private function getModuleIdFromModulePath($modulePath)
    {
        $modulePath = substr($modulePath, strlen($_SERVER['DOCUMENT_ROOT']));
        $arPaths = explode(DIRECTORY_SEPARATOR, $modulePath);
        $pos = array_search('modules', $arPaths);
        $modulePos = $pos + 1;
        $moduleId = $arPaths[$modulePos];
        return $moduleId ?: '';
    }

    public function DoInstall()
    {
        global $APPLICATION, $errors;

        if (!ModuleManager::isModuleInstalled('iblock')) {
            $errors = Loc::getMessage('RAPID_USERTYPES_REQUIRE_IBLOCK_MODULE');
        }
        if (!$this->InstallFiles()) {
            return;
        }
        $this->InstallEvents();
        if (!$errors) {
            RegisterModule($this->MODULE_ID);
        } else {
            $APPLICATION->IncludeAdminFile(
                Loc::getMessage('RAPID_USERTYPES_RESULT_PAGE_TITLE'),
                __DIR__ . '/install_result.php'
            );
        }
    }

    public function InstallFiles()
    {
        return $this;
    }

    public function InstallEvents()
    {
        RegisterModuleDependences("main", "OnBeforeProlog", $this->MODULE_ID); // autoload
        return $this;
    }

    public function DoUninstall()
    {
        $this
            ->UnInstallEvents()
            ->UnInstallFiles();
        UnRegisterModule($this->MODULE_ID);
    }

    public function UnInstallEvents()
    {
        global $DB;
        $dbModuleDepends = $DB->Query('SELECT * FROM b_module_to_module WHERE TO_MODULE_ID = "' . $this->MODULE_ID . '"');
        while ($arModuleDepend = $dbModuleDepends->Fetch()) {
            UnRegisterModuleDependences(
                $arModuleDepend['FROM_MODULE_ID'],
                $arModuleDepend['MESSAGE_ID'],
                $arModuleDepend['TO_MODULE_ID'],
                $arModuleDepend['TO_CLASS'],
                $arModuleDepend['TO_METHOD']
            );
        }
        return $this;
    }

    public function UnInstallFiles()
    {
        return $this;
    }

}
