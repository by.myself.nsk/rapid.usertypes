<?php
/** @var array $MESS */
$MESS = array_merge($MESS, array(
    'RAPID_USERTYPES_MODULE_NAME' => 'Rapid: Пользовательские типы и свойства',
    'RAPID_USERTYPES_MODULE_DESCRIPTION' => 'Сборка дополнительных свойств и пользовательских типов для расширения функционала',
    'RAPID_USERTYPES_PARTNER_NAME' => 'Rapid',
    'RAPID_USERTYPES_PARTNER_URI' => '',
    'RAPID_USERTYPES_REQUIRE_IBLOCK_MODULE' => 'Для установки необходим установленный модуль iblock',
    'RAPID_USERTYPES_RESULT_PAGE_TITLE' => 'Установка модуля Rapid: Пользовательские типы и свойства',
));
